use platzi;
-- unsigned = no guarda el simbolo + o -
CREATE TABLE books (
	book_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    publisher_id INTEGER UNSIGNED NOT NULL,
    author VARCHAR(100) NOT NULL,
    title	VARCHAR(60) NOT NULL,
    description TEXT,
    price DECIMAL(5,2),
    copies INT NOT NULL DEFAULT 0
    
);
-- DROP TABLE books;

CREATE TABLE publishers(
	publisher_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    country VARCHAR(20)
);

CREATE TABLE users (
	user_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE
);

-- acciones del usuario
-- ENUM() lista de valores que se aceptaran 
CREATE TABLE actions (
	action_id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    book_id INTEGER UNSIGNED NOT NULL,
    user_id INTEGER UNSIGNED NOT NULL,
    action_type ENUM('venta','prestamo','devolucion') NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- ------------------Datos--------------------------------------
INSERT INTO users(name, email) VALUES
    ('Ricardo', 'ricardo@hola.com'),
    ('Laura', 'laura@hola.com'),
    ('Jose', 'jose@hola.com'),
    ('Sofia', 'sofia@hola.com'),
    ('Fernanda', 'fernanda@hola.com'),
    ('Jose Guillermo', 'memo@hola.com'),
    ('Maria', 'maria@hola.com'),
    ('Susana', 'susana@hola.com'),
    ('Jorge', 'jorge@hola.com');

INSERT INTO publishers(publisher_id, name, country) VALUES
    (1, 'OReilly', 'USA'),
    (2, 'Santillana', 'Mexico'),
    (3, 'MIT Edu', 'USA'),
    (4, 'UTPC', 'Colombia'),
    (5, 'Platzi', 'USA');
    
INSERT INTO books(publisher_id, title, author, description, price, copies) VALUES
    (1, 'Mastering MySQL', 'John Goodman', 'Clases de bases de datos usando MySQL', 10.50, 4),
    (2, 'Trigonometria avanzada', 'Pi Tagoras', 'Trigonometria desde sus origenes', 7.30, 2),
    (3, 'Advanced Statistics', 'Carl Gauss', 'De curvas y otras graficas', 23.60, 1),
    (4, 'Redes Avanzadas', 'Tim Bernes-Lee', 'Lo que viene siendo el Internet', 13.50, 4),
    (2, 'Curvas Parabolicas', 'Napoleon TNT', 'Historia de la parabola', 6.99, 10),
    (1, 'Ruby On (the) Road', 'A Miner', 'Un nuevo acercamiento a la programacion', 18.75, 4),
    (1, 'Estudios basicos de estudios', 'John Goodman', 'Clases de datos usando MySQL', 10.50 , 4),
    (4, 'Donde esta Y?', 'John Goodman', 'Clases de datos usando MySQL', 10.50, 4),
    (3, 'Quimica Avanzada', 'John White', 'Profitable studies on chemistry', 45.35, 1),
    (4, 'Graficas Matematicas', 'Rene Descartes', 'De donde viene el plano', 13.99, 7),
    (4, 'Numeros Importantes', 'Leonard Euler', 'De numeros que a veces nos sirven', 10, 3),
    (3, 'Modelado de conocimiento', 'Jack Friedman', 'Una vez adquirido, como se guarda el conocimiento', 29.99, 2),
    (3, 'Teoria de juegos', 'John Nash', 'A o B?', 12.55, 3),
    (1, 'Calculo de variables', 'Brian Kernhigan', 'Programacion mega basica', 9.50, 3),
    (5, 'Produccion de streaming', 'Juan Pablo Rojas', 'De la oficina ala pan', 23.49, 9),
    (5, 'ELearning', 'JFD & DvdH', 'Diseno y ejecucion de educacion online', 23.55, 4),
    (5, 'Pet Caring for Geeks', 'KC', 'Que tu perro aprenda a programar', 18.79, 3 ),
    (1, 'Algebra basica', 'Al Juarismi', 'Esto de encontrar X o Y, dependiendo', 13.50, 8);
    
INSERT INTO actions(book_id, user_id, action_type) VALUES 
	(3, 2, 'venta'),
	(6, 1, 'devolucion'),
	(7, 7, 'devolucion'),
	(2, 5, 'venta'),
	(10, 9, 'venta'),
	(18, 8, 'devolucion'),
	(12, 4, 'venta'),
	(1, 3, 'venta'),
	(4, 5, 'devolucion'),
	(5, 2, 'venta');
    
SELECT a.action_id, 
		b.title, 
		a.action_type, 
        u.name,
--        b.price
-- 		0 as price
		if(a.action_type = 'venta', b.price, 0) as price,
        b.book_id as bid,
        if(b.book_id IN (1,4,7,8,2),b.price * 0.9, b.price) AS dcto
FROM actions AS a
LEFT JOIN books as b
	ON b.book_id = a.book_id
LEFT JOIN users as u
	on a.user_id = u.user_id;
-- WHERE a.action_type IN ('prestamo', 'devolucion');
-- WHERE a.action_type <> 'venta';
-- WHERE a.action_type = 'venta';


-- UNION une dos resultado de querys en una tabla, requisitos tiene que tener el mismo numero de columnas
-- ORDER BY


-- querys
-- query ordenar por editorila mas suma de precios
SELECT p.publisher_id AS pid, 
	p.name, 
    SUM(IF(b.price < 15, 0, b.price * b.copies)) AS total
FROM books AS b
JOIN publishers AS p
	ON b.publisher_id = p.publisher_id
GROUP BY pid;


SELECT p.publisher_id AS pid, 
	p.name, 
    SUM(IF(b.price > 15,0,b.price*b.copies)) AS total,
    SUM(IF(b.price < 15,0,1)) AS libros_por_vender,
    COUNT(b.book_id) AS libros
FROM books AS b
JOIN publishers AS p 
ON  b.publisher_id = p.publisher_id
GROUP BY pid;

-- insertar informacion-- 
INSERT INTO users(name, email)
	VALUES('Susana','correo@gmail.com')
ON DUPLICATE KEY
UPDATE
	active = active + 1,
	name = CONCAT(name, ' - nuevo');


-- modificar un registro-- 
UPDATE users SET name='Mynor' 
WHERE user_id = 4
LIMIT 1;

-- replace puede tomar sintaxis de insertar o update
REPLACE INTO users(name, email, active)
	VALUES('lorena','laura@hola.com', 4);
    
REPLACE INTO users SET name='Jose', email='jose@hola.com' ;   